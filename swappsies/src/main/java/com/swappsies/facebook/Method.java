/*
 * Copyright (c) 2015. $user
 */

package com.swappsies.facebook;

/**
 * LOGIN : Facebook login,
 * <p/>
 * SHARE : Share to facebook wall post,
 * <p/>
 * PROFILE : Getting user profile detail.
 */
public enum Method {
    /**
     * Login
     */
    LOGIN,
    /**
     * Share to wall post
     */
    SHARE,
    /**
     * Fetch user profile
     */

    PROFILE
}
