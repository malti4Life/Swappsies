package com.swappsies.Activities;

import com.swappsies.R;

/**
 * Created by MaltiDevnani on 3/1/2017.
 */

public class WelcomeActivity extends BaseActivity {
    @Override
    protected void initView() {

    }

    @Override
    protected void initToolBar() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_welcom;
    }
}
