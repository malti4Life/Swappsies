package com.swappsies;


import android.app.Application;

import com.swappsies.Webservices.RestClient;

public class SwappsiesApplication extends Application{
    private static SwappsiesApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;



        new RestClient();
    }
}
